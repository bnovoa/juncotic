#!/bin/bash

# EN AMBOS FIREWALLs

LAN="192.168.10.0/24"
DMZ="192.168.20.0/24"

LAN_IP="192.168.20.1"
DMZ_IP="192.168.20.1"

DMZ_WEB="192.168.20.10"
DMZ_MYSQL="192.168.20.20"

IP_FW1_LAN="192.168.10.1"
IP_FW1_DMZ="192.168.20.1"
IP_FW2_DMZ="192.168.20.2"
IP_FW2_WAN="200.3.4.5"

FW1_LAN="eth0"
FW1_DMZ="eth1"
FW2_DMZ="eth0"
FW2_WAN="eth1"

INET_CLIENT="11.22.33.44"

iptables -F
iptables -t nat -F
iptables -Z
iptables -P INPUT -j DROP
iptables -P OUTPUT -j DROP
iptables -P FORWARD -j DROP

# Habilitamos trafico local
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# Habilitamos forwarding en las interfaces de red
echo 1 >/proc/sys/net/ipv4/ip_forward

# Habilitamos gestion de estados
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT

# Habilitamos el NAT saliente (solo en el firewall 2)
iptables -t nat -A POSTROUTING -o $ETH_WAN -j SNAT --to-source $IP_FW2_WAN

# --------------------------


# 1)
# En ambos Firewalls
iptables -A FORWARD -s $LAN -p tcp -m multiport --dports 80,443,53 -j ACCEPT
iptables -A FORWARD -s $LAN -p udp --dport 53 -j ACCEPT
iptables -A FORWARD -s $LAN -p tcp -m multiport --dports 110,995 -j ACCEPT
iptables -A FORWARD -s $LAN -p tcp -m multiport --dports 143,993 -j ACCEPT
iptables -A FORWARD -s $LAN -p tcp -m multiport --dports 25,2525,465 -j ACCEPT

# 2) En el firewall 1
iptables -A FORWARD -s $LAN -d $DMZ_WEB -p tcp --dport 443 -j ACCEPT

# 3)
# En el firewall 1
iptables -A FORWARD -s $LAN -d $IP_FW2 -p tcp --dport 22 -j ACCEPT
iptables -A INPUT -s $LAN -p tcp --dport 22 -j ACCEPT

# 4)
# En el firewall 2
iptables -t -nat -A PREROUTING -p tcp --dport 443 -j DNAT --to-destination $DMZ_WEB:443
iptables -A FORWARD -d $DMZ_WEB -p tcp --dport 443 -j ACCEPT

# 5) 
# En el firewall 1
iptables -A FORWARD -s $LAN -d $DMZ_MYSQL -p tcp --dport 3306 -j ACCEPT

# 6) Por politicas por defecto

# 7)
# Desde Internet se deberá invocar al puerto 22 para acceder al firewall 2, y el puerto 2222 para el firewall 1

# En el firewall 2
iptables -A INPUT -s $INET_CLIENT -p tcp --dport 22 -j ACCEPT
iptables -t -nat -A PREROUTING -p tcp --dport 2222 -j DNAT --to-destination $IP_FW1_DMZ:22

# En el firewall 1:
iptables -A FORWARD -s $INET_CLIENT -p tcp --dport 22 -j ACCEPT


